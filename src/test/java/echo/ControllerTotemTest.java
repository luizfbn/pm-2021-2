package echo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import echo.util.JavalinApp;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

class ControllerTotemTest {

	private static JavalinApp app = new JavalinApp(); // inject any dependencies you might have

    @BeforeAll
    static void init() {
        app.start(7010);
    }

    @AfterAll
    static void afterAll(){
        app.stop();
    }

	@Test
    void recuperarTotensTest() {
        HttpResponse response = Unirest.get("http://localhost:7010/totem").asString();
        assertEquals(200, response.getStatus());
    }

	@Test
    void incluirTotemTest() {
		String jsonCompletoTeste = "{ \"localizacao\": \"teste localizacao\" }";
		String jsonIncompletoTeste = "{}";

		HttpResponse response = Unirest.post("http://localhost:7010/totem").body(jsonCompletoTeste).asString();
		assertEquals(200, response.getStatus());

		HttpResponse responseError = Unirest.post("http://localhost:7010/totem").body(jsonIncompletoTeste).asString();
		assertEquals(422, responseError.getStatus());
    }
	/*
	@Test
    void editarTotemTest() {
		ControllerTotem totemTeste = new ControllerTotem("teste localizacao");
		ControllerTotem.totens.add(totemTeste);
		String jsonCompletoTeste = "{ \"localizacao\": \"teste localizacao 1\" }";
		String jsonIncompletoTeste = "{ }";

		HttpResponse response = Unirest.put("http://localhost:7010/totem/" + totemTeste.id).body(jsonCompletoTeste).asString();
		assertEquals(200, response.getStatus());

        HttpResponse responseNotFoundError = Unirest.put("http://localhost:7010/totem/1").body(jsonCompletoTeste).asString();
        assertEquals(404, responseNotFoundError.getStatus());

        HttpResponse responseError = Unirest.put("http://localhost:7010/totem/" + totemTeste.id).body(jsonIncompletoTeste).asString();
        assertEquals(422, responseError.getStatus());
    }
	*/
	@Test
    void removerTotemTest() {
		ControllerTotem totemTeste = new ControllerTotem("teste localizacao");
		ControllerTotem.totens.add(totemTeste);

		HttpResponse response = Unirest.delete("http://localhost:7010/totem/" + totemTeste.id).asString();
		assertEquals(200, response.getStatus());

        HttpResponse responseNotFoundError = Unirest.delete("http://localhost:7010/totem/1").asString();
        assertEquals(404, responseNotFoundError.getStatus());
    }

	@Test
    void listarTrancasTest() {
		ControllerTotem totemTeste = new ControllerTotem("teste localizacao");
		ControllerTotem.totens.add(totemTeste);

		HttpResponse response = Unirest.get("http://localhost:7010/totem/" + totemTeste.id + "/trancas").asString();
        assertEquals(200, response.getStatus());

        HttpResponse responseNotFoundError = Unirest.get("http://localhost:7010/totem/1/trancas").asString();
        assertEquals(404, responseNotFoundError.getStatus());
    }

	@Test
    void listarBicicletasTest() {
		ControllerTotem totemTeste = new ControllerTotem("teste localizacao");
		ControllerTotem.totens.add(totemTeste);

		HttpResponse response = Unirest.get("http://localhost:7010/totem/" + totemTeste.id + "/bicicletas").asString();
        assertEquals(200, response.getStatus());

        HttpResponse responseNotFoundError = Unirest.get("http://localhost:7010/totem/1/bicicletas").asString();
        assertEquals(404, responseNotFoundError.getStatus());
    }

	@Test
    void obterTotemPorIdTest() {
    	ControllerTotem totem = ControllerTotem.obterTotemPorId("teste");
    	assertEquals(null, totem);
    }
}
