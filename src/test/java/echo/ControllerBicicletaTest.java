package echo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import echo.util.JavalinApp;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

class ControllerBicicletaTest {

	private static JavalinApp app = new JavalinApp(); // inject any dependencies you might have

    @BeforeAll
    static void init() {
        app.start(7010);
    }

    @AfterAll
    static void afterAll(){
        app.stop();
    }

    @Test
    void recuperarBicicletaTest() {
        HttpResponse response = Unirest.get("http://localhost:7010/bicicleta").asString();
        assertEquals(200, response.getStatus());
    }

    @Test
    void cadastrarBicicletaTest() {
    	String jsonCompletoTeste = "{ \"marca\": \"teste marca 1\", \"modelo\": \"teste modelo\", \"ano\": \"teste ano\", \"status\": \"teste status\", \"numero\": 1}";

    	HttpResponse response = Unirest.post("http://localhost:7010/bicicleta").body(jsonCompletoTeste).asString();
    	assertEquals(200, response.getStatus());

    	String jsonIncompletoTeste = "{ \"modelo\": \"teste modelo\", \"ano\": \"teste ano\", \"status\": \"teste status\", \"numero\": 1}";
    	HttpResponse responseError = Unirest.post("http://localhost:7010/bicicleta").body(jsonIncompletoTeste).asString();
    	assertEquals(422, responseError.getStatus());
    }
    
    @Test
    void intregarBicicletaTest() {
    	ControllerBicicleta bicicletaTeste = new ControllerBicicleta("teste marca", "teste modelo", "teste ano", "teste status", 2);
        ControllerBicicleta.bicicletas.add(bicicletaTeste);
        ControllerTranca trancaTeste = new ControllerTranca("teste localizacao", "teste ano", "teste modelo", "teste status", 2);
    	ControllerTranca.trancas.add(trancaTeste);
    	String jsonCompletoTeste = "{ \"idTranca\": " + "\""+ trancaTeste.id + "\"," + "\"idBicicleta\": " + "\""+ bicicletaTeste.id + "\"}";
    	String jsonIncompletoTeste = "{ \"idTranca\": " + "\"123\"," + "\"idBicicleta\": " + "\""+ bicicletaTeste.id + "\"}";

    	HttpResponse response = Unirest.post("http://localhost:7010/bicicleta/integrarNaRede").body(jsonCompletoTeste).asString();
    	assertEquals(200, response.getStatus());

    	HttpResponse responseError = Unirest.post("http://localhost:7010/bicicleta/integrarNaRede").body(jsonIncompletoTeste).asString();
    	assertEquals(422, responseError.getStatus());
    }

    @Test
    void retirarBicicletaTest() {
    	ControllerBicicleta bicicletaTeste = new ControllerBicicleta("teste marca", "teste modelo", "teste ano", "teste status", 2);
        ControllerTranca trancaTeste = new ControllerTranca("teste localizacao", "teste ano", "teste modelo", "teste status", 2);
        bicicletaTeste.tranca = trancaTeste.id;
        trancaTeste.bicicleta = bicicletaTeste.id;
        ControllerBicicleta.bicicletas.add(bicicletaTeste);
    	ControllerTranca.trancas.add(trancaTeste);
    	String jsonCompletoTeste = "{ \"idTranca\": " + "\""+ trancaTeste.id + "\"," + "\"idBicicleta\": " + "\""+ bicicletaTeste.id + "\"}";
    	String jsonIncompletoTeste = "{ \"idTranca\": " + "\"123\"," + "\"idBicicleta\": " + "\""+ bicicletaTeste.id + "\"}";

    	HttpResponse response = Unirest.post("http://localhost:7010/bicicleta/retirarDaRede").body(jsonCompletoTeste).asString();
    	assertEquals(200, response.getStatus());

    	HttpResponse responseError = Unirest.post("http://localhost:7010/bicicleta/retirarDaRede").body(jsonIncompletoTeste).asString();
    	assertEquals(422, responseError.getStatus());
    }

    @Test
    void obterBicicletaTest() {
        ControllerBicicleta bicicletaTeste = new ControllerBicicleta("teste marca", "teste modelo", "teste ano", "teste status", 2);
        ControllerBicicleta.bicicletas.add(bicicletaTeste);

        HttpResponse response = Unirest.get("http://localhost:7010/bicicleta/" + bicicletaTeste.id).asString();
        assertEquals(200, response.getStatus());

        HttpResponse responseNotFoundError = Unirest.get("http://localhost:7010/bicicleta/1").asString();
        assertEquals(404, responseNotFoundError.getStatus());
    }

    @Test
    void editarBicicletaTest() {
    	ControllerBicicleta bicicletaTeste = new ControllerBicicleta("teste marca", "teste modelo", "teste ano", "teste status", 2);
        ControllerBicicleta.bicicletas.add(bicicletaTeste);
        String jsonCompletoTeste = "{ \"marca\": \"teste marca 1\", \"modelo\": \"teste modelo\", \"ano\": \"teste ano\", \"status\": \"teste status\", \"numero\": 2}";
        String jsonIncompletoTeste = "{ \"modelo\": \"teste modelo\", \"ano\": \"teste ano\", \"status\": \"teste status\", \"numero\": 2}";

        HttpResponse response = Unirest.put("http://localhost:7010/bicicleta/" + bicicletaTeste.id).body(jsonCompletoTeste).asString();
        assertEquals(200, response.getStatus());

    	HttpResponse responseNotFoundError = Unirest.put("http://localhost:7010/bicicleta/1").body(jsonCompletoTeste).asString();
        assertEquals(404, responseNotFoundError.getStatus());

        HttpResponse responseError = Unirest.put("http://localhost:7010/bicicleta/" + bicicletaTeste.id).body(jsonIncompletoTeste).asString();
        assertEquals(422, responseError.getStatus());
    }

    @Test
    void removerBicicletaTest() {
    	ControllerBicicleta bicicletaTeste = new ControllerBicicleta("teste marca", "teste modelo", "teste ano", "teste status", 2);
        ControllerBicicleta.bicicletas.add(bicicletaTeste);

        HttpResponse response = Unirest.delete("http://localhost:7010/bicicleta/" + bicicletaTeste.id).asString();
        assertEquals(200, response.getStatus());

    	HttpResponse responseNotFoundError = Unirest.delete("http://localhost:7010/bicicleta/1").asString();
        assertEquals(404, responseNotFoundError.getStatus());
    }

    @Test
    void statusBicicletaTest() {
    	ControllerBicicleta bicicletaTeste = new ControllerBicicleta("teste marca", "teste modelo", "teste ano", "teste status", 2);
        ControllerBicicleta.bicicletas.add(bicicletaTeste);

        HttpResponse response = Unirest.post("http://localhost:7010/bicicleta/" + bicicletaTeste.id + "/status/disponivel").asString();
        assertEquals(200, response.getStatus());

    	HttpResponse responseNotFoundError = Unirest.post("http://localhost:7010/bicicleta/1/status/disponivel").asString();
        assertEquals(404, responseNotFoundError.getStatus());

        HttpResponse responseError = Unirest.post("http://localhost:7010/bicicleta/" + bicicletaTeste.id + "/status/dispo").asString();
        assertEquals(422, responseError.getStatus());
    }

    @Test
    void obterBicicletaPorIdTest() {
    	ControllerBicicleta bicicleta = ControllerBicicleta.obterBicicletaPorId("teste");
    	assertEquals(null, bicicleta);
    }
}
