package echo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import echo.util.JavalinApp;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

class ControllerTrancaTest {

	private static JavalinApp app = new JavalinApp(); // inject any dependencies you might have

    @BeforeAll
    static void init() {
        app.start(7010);
    }

    @AfterAll
    static void afterAll(){
        app.stop();
    }

    @Test
    void recuperarTrancasTest() {
        HttpResponse response = Unirest.get("http://localhost:7010/tranca").asString();
        assertEquals(200, response.getStatus());
    }

    @Test
    void cadastrarTrancaTest() {
    	String jsonCompletoTeste = "{ \"numero\": 1, \"localizacao\": \"teste localizacao\", \"anoDeFabricacao\": \"teste ano\", \"modelo\": \"teste modelo\", \"status\": \"teste status\"}";
    	String jsonIncompletoTeste = "{ \"localizacao\": \"teste localizacao\", \"anoDeFabricacao\": \"teste ano\", \"modelo\": \"teste modelo\", \"status\": \"teste status\"}";

    	HttpResponse response = Unirest.post("http://localhost:7010/tranca").body(jsonCompletoTeste).asString();
    	assertEquals(200, response.getStatus());

    	HttpResponse responseError = Unirest.post("http://localhost:7010/tranca").body(jsonIncompletoTeste).asString();
    	assertEquals(422, responseError.getStatus());
    }

    @Test
    void integrarTrancaTest() {
    	ControllerTotem totemTeste = new ControllerTotem("teste localizacao");
		ControllerTotem.totens.add(totemTeste);
        ControllerTranca trancaTeste = new ControllerTranca("teste localizacao", "teste ano", "teste modelo", "teste status", 2);
    	ControllerTranca.trancas.add(trancaTeste);
    	String jsonCompletoTeste = "{ \"idTranca\": " + "\""+ trancaTeste.id + "\"," + "\"idTotem\": " + "\""+ totemTeste.id + "\"}";
    	String jsonIncompletoTeste = "{ \"idTranca\": " + "\"123\"," + "\"idTotem\": " + "\""+ totemTeste.id + "\"}";

    	HttpResponse response = Unirest.post("http://localhost:7010/tranca/integrarNaRede").body(jsonCompletoTeste).asString();
        assertEquals(200, response.getStatus());

        HttpResponse responseError = Unirest.post("http://localhost:7010/tranca/integrarNaRede").body(jsonIncompletoTeste).asString();
        assertEquals(422, responseError.getStatus());
    }

    @Test
    void retirarTrancaTest() {
    	ControllerTotem totemTeste = new ControllerTotem("teste localizacao");
        ControllerTranca trancaTeste = new ControllerTranca("teste localizacao", "teste ano", "teste modelo", "teste status", 2);
        totemTeste.trancasTotem.add(trancaTeste);
        ControllerTotem.totens.add(totemTeste);
    	ControllerTranca.trancas.add(trancaTeste);
    	String jsonCompletoTeste = "{ \"idTranca\": " + "\""+ trancaTeste.id + "\"," + "\"idTotem\": " + "\""+ totemTeste.id + "\"}";
    	String jsonIncompletoTeste = "{ \"idTranca\": " + "\"123\"," + "\"idTotem\": " + "\""+ totemTeste.id + "\"}";

    	HttpResponse response = Unirest.post("http://localhost:7010/tranca/retirarDaRede").body(jsonCompletoTeste).asString();
        assertEquals(200, response.getStatus());

        HttpResponse responseError = Unirest.post("http://localhost:7010/tranca/retirarDaRede").body(jsonIncompletoTeste).asString();
        assertEquals(422, responseError.getStatus());
    }

    @Test
    void obterTrancaTest() {
    	ControllerTranca trancaTeste = new ControllerTranca("teste localizacao", "teste ano", "teste modelo", "teste status", 2);
    	ControllerTranca.trancas.add(trancaTeste);

        HttpResponse response = Unirest.get("http://localhost:7010/tranca/" + trancaTeste.id).asString();
        assertEquals(200, response.getStatus());

        HttpResponse responseNotFoundError = Unirest.get("http://localhost:7010/tranca/1").asString();
        assertEquals(404, responseNotFoundError.getStatus());
    }

    @Test
    void editarTrancaTest() {
    	ControllerTranca trancaTeste = new ControllerTranca("teste localizacao", "teste ano", "teste modelo", "teste status", 2);
    	ControllerTranca.trancas.add(trancaTeste);
    	String jsonCompletoTeste = "{ \"numero\": 1, \"localizacao\": \"teste localizacao 1\", \"anoDeFabricacao\": \"teste ano\", \"modelo\": \"teste modelo\", \"status\": \"teste status\"}";
    	String jsonIncompletoTeste = "{ \"localizacao\": \"teste localizacao 1\", \"anoDeFabricacao\": \"teste ano\", \"modelo\": \"teste modelo\", \"status\": \"teste status\"}";

    	HttpResponse response = Unirest.put("http://localhost:7010/tranca/" + trancaTeste.id).body(jsonCompletoTeste).asString();
        assertEquals(200, response.getStatus());

        HttpResponse responseNotFoundError = Unirest.put("http://localhost:7010/tranca/1").body(jsonCompletoTeste).asString();
        assertEquals(404, responseNotFoundError.getStatus());

        HttpResponse responseError = Unirest.put("http://localhost:7010/tranca/" + trancaTeste.id).body(jsonIncompletoTeste).asString();
        assertEquals(422, responseError.getStatus());
    }

    @Test
    void removerTrancaTest() {
    	ControllerTranca trancaTeste = new ControllerTranca("teste localizacao", "teste ano", "teste modelo", "teste status", 2);
    	ControllerTranca.trancas.add(trancaTeste);

        HttpResponse response = Unirest.delete("http://localhost:7010/tranca/" + trancaTeste.id).asString();
        assertEquals(200, response.getStatus());

        HttpResponse responseNotFoundError = Unirest.delete("http://localhost:7010/tranca/1").asString();
        assertEquals(404, responseNotFoundError.getStatus());
    }
    
    @Test
    void statusTrancaTest() {
    	ControllerTranca trancaTeste = new ControllerTranca("teste localizacao", "teste ano", "teste modelo", "teste status", 2);
    	ControllerTranca.trancas.add(trancaTeste);
    	
    	HttpResponse response = Unirest.post("http://localhost:7010/tranca/" + trancaTeste.id + "/status/trancar").asString();
        assertEquals(200, response.getStatus());

    	HttpResponse responseNotFoundError = Unirest.post("http://localhost:7010/tranca/1/status/trancar").asString();
        assertEquals(404, responseNotFoundError.getStatus());

        HttpResponse responseError = Unirest.post("http://localhost:7010/tranca/" + trancaTeste.id + "/status/tran").asString();
        assertEquals(422, responseError.getStatus());
    }
    
    @Test
    void obterBicicletaTest() {
    	ControllerBicicleta bicicletaTeste = new ControllerBicicleta("teste marca", "teste modelo", "teste ano", "teste status", 2);
        ControllerTranca trancaTeste = new ControllerTranca("teste localizacao", "teste ano", "teste modelo", "teste status", 2);
        bicicletaTeste.tranca = trancaTeste.id;
        trancaTeste.bicicleta = bicicletaTeste.id;
        ControllerBicicleta.bicicletas.add(bicicletaTeste);
    	ControllerTranca.trancas.add(trancaTeste);

    	HttpResponse response = Unirest.get("http://localhost:7010/tranca/" + trancaTeste.id + "/bicicleta").asString();
        assertEquals(200, response.getStatus());

        HttpResponse responseNotFoundError = Unirest.get("http://localhost:7010/tranca/1/bicicleta").asString();
        assertEquals(404, responseNotFoundError.getStatus());
    }

    @Test
    void obterTrancaPorIdTest() {
    	ControllerTranca tranca = ControllerTranca.obterTrancaPorId("teste");
    	assertEquals(null, tranca);
    }
}
