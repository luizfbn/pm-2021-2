package echo.util;

import io.javalin.Javalin;
import static io.javalin.apibuilder.ApiBuilder.*;
import echo.Controller;
import echo.ControllerBicicleta;
import echo.ControllerTotem;
import echo.ControllerTranca;

public class JavalinApp {
    private Javalin app = 
            Javalin.create(config -> config.defaultContentType = "application/json")
                .routes(() -> {
                	path("/bicicleta", () -> {
                		get(ControllerBicicleta::recuperarBicicletas);
                		post(ControllerBicicleta::cadastrarBicicleta);
                		path(":idBicicleta", () -> {
                			get(ControllerBicicleta::obterBicicleta);
                			put(ControllerBicicleta::editarBicicleta);
                			delete(ControllerBicicleta::removerBicicleta);
                			path("status/:acao", () -> post(ControllerBicicleta::statusBicicleta));
                		});
                		path("integrarNaRede", () -> post(ControllerBicicleta::intregarBicicleta));
                    	path("retirarDaRede", () -> post(ControllerBicicleta::retirarBicicleta));
                	});
                	path("/totem", () -> {
                		get(ControllerTotem::recuperarTotens);
                		post(ControllerTotem::incluirTotem);
                		path(":idTotem", () -> {
                			put(ControllerTotem::editarTotem);
                			delete(ControllerTotem::removerTotem);
                			path("trancas", () -> get(ControllerTotem::listarTrancas));
                			path("bicicletas", () -> get(ControllerTotem::listarBicicletas));
                		});
                		
                	} );
                	path("/tranca", () -> {
                		get(ControllerTranca::recuperarTrancas);
                		post(ControllerTranca::cadastrarTranca);
                		path(":idTranca", () -> {
                			get(ControllerTranca::obterTranca);
                			put(ControllerTranca::editarTranca);
                			delete(ControllerTranca::removerTranca);
                			path("bicicleta", () -> get(ControllerTranca::obterBicicleta));
                			path("status/:acao", () -> post(ControllerTranca::statusTranca));
                		});
                		path("integrarNaRede", () -> post(ControllerTranca::integrarTranca));
                    	path("retirarDaRede", () -> post(ControllerTranca::retirarTranca));
                	});
                    path("/:echo", () -> get(Controller::getEcho));
                    path("/", ()-> get(Controller::getRoot));
                    });

    public void start(int port) {
        this.app.start(port);
    }

    public void stop() {
        this.app.stop();
    }
}
