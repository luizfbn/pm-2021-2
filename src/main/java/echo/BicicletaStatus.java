package echo;

public enum BicicletaStatus {
	DISPONIVEL, EM_USO, NOVA, APOSENTADA, REPARO_SOLICITADO, EM_REPARO;
}
