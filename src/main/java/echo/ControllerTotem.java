package echo;

import java.util.ArrayList;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.javalin.http.Context;

public class ControllerTotem {

	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	public String id = UUID.randomUUID().toString();
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	public ArrayList <ControllerTranca> trancasTotem = new ArrayList <ControllerTranca>();

	public String localizacao;

	private static final String ID_TOTEM = "idTotem";

	public static ArrayList <ControllerTotem> totens = new ArrayList <ControllerTotem>();

	public ControllerTotem(@JsonProperty("localizacao") String localizacao) {
		this.localizacao = localizacao;
	}

	public static void recuperarTotens(Context ctx) {
		ctx.status(200).json(ControllerTotem.totens);
    }

	public static void incluirTotem(Context ctx) {
		// o status deve ser "nova" (não pode ser editado) (?) -- NÃO DESENVOLVIDO
		try {
			ctx.bodyValidator(ControllerTotem.class)
				.check(obj -> obj.localizacao != null && !obj.localizacao.trim().isEmpty())
			    .get();

			ControllerTotem.totens.add(ctx.bodyAsClass(ControllerTotem.class));

			ctx.status(200).json(ControllerTotem.totens.get(ControllerTotem.totens.size() - 1));
		} catch (Exception e) {
			ctx.status(422).result(MensagemResposta.DADOS_INVALIDOS.mensagem);
		}
    }

	public static void editarTotem(Context ctx) {
		try {
			ctx.bodyValidator(ControllerTotem.class)
				.check(obj -> obj.localizacao != null && !obj.localizacao.trim().isEmpty())
			    .get();

			ControllerTotem totem = ControllerTotem.obterTotemPorId(ctx.pathParam(ID_TOTEM));

			if (totem != null) {
				int index = ControllerTotem.totens.indexOf(totem);
				String idTotem = totem.id;

				totem = ctx.bodyAsClass(ControllerTotem.class);
				totem.id = idTotem;

				ControllerTotem.totens.set(index, totem);

				ctx.status(200).json(totem);
				return;
			}
	        ctx.status(404).result(MensagemResposta.NAO_ENCONTRADO.mensagem);
		} catch (Exception e) {
			ctx.status(422).result(MensagemResposta.DADOS_INVALIDOS.mensagem);
		}
    }

	public static void removerTotem(Context ctx) {
		// apenas totens sem trancas podem ser excluidos -- OK
		ControllerTotem totem = ControllerTotem.obterTotemPorId(ctx.pathParam(ID_TOTEM));

		if (totem != null && totem.trancasTotem.size() < 1) {
			ControllerTotem.totens.remove(totem);

			ctx.status(200).result("Totem removido");
			return;
		}
		ctx.status(404).result(MensagemResposta.NAO_ENCONTRADO.mensagem);
    }

	public static void listarTrancas(Context ctx) {
		ControllerTotem totem = ControllerTotem.obterTotemPorId(ctx.pathParam(ID_TOTEM));

		if (totem != null) {
			ctx.status(200).json(totem.trancasTotem);
			return;
		}
		ctx.status(404).result(MensagemResposta.NAO_ENCONTRADO.mensagem);
    }

	public static void listarBicicletas(Context ctx) {
		ControllerTotem totem = ControllerTotem.obterTotemPorId(ctx.pathParam(ID_TOTEM));

		if (totem != null) {
			ArrayList <ControllerBicicleta> bicicletasTotem = new ArrayList <ControllerBicicleta>();
			for (ControllerTranca tranca : totem.trancasTotem) {
				if (tranca.bicicleta != null) {
					bicicletasTotem.add(ControllerBicicleta.obterBicicletaPorId(tranca.bicicleta));
				}
			}
			ctx.status(200).json(bicicletasTotem);
			return;
		}
        ctx.status(404).result(MensagemResposta.NAO_ENCONTRADO.mensagem);
    }

	// Funções auxiliares

	public static ControllerTotem obterTotemPorId(String idTotem) {
		for (ControllerTotem totem : totens) {
			if (totem.id.equals(idTotem)) {
				return totem;
			}
		}
		return null;
	}
}
