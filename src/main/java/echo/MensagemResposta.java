package echo;

public enum MensagemResposta {
	NAO_ENCONTRADO("Não encontrado(a)"),
	DADOS_INVALIDOS("Dados inválidos");
	
	String mensagem;
	 
	private MensagemResposta(String mensagem) {
		this.mensagem = mensagem;
	}
 
	public String getMensagem() {
		return mensagem;
	}
}
