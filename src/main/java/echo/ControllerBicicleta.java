package echo;

import java.util.ArrayList;
import java.util.UUID;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.javalin.http.Context;

public class ControllerBicicleta {

	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	public String id = UUID.randomUUID().toString();
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	public String tranca;

	public String marca;
	public String modelo;
	public String ano;
	public String status;
	public int numero;

	private static final String ID_BICICLETA = "idBicicleta";

	public static ArrayList <ControllerBicicleta> bicicletas = new ArrayList <ControllerBicicleta>();

	public ControllerBicicleta(
			@JsonProperty("marca") String marca, 
			@JsonProperty("modelo") String modelo, 
			@JsonProperty("ano") String ano, 
			@JsonProperty("status") String status, 
			@JsonProperty("numero") int numero
			) {
		this.marca = marca;
		this.modelo = modelo;
		this.ano = ano;
		this.status = status;
		this.numero = numero;
	}

	public static void recuperarBicicletas(Context ctx) {
		ctx.json(ControllerBicicleta.bicicletas).status(200);
    }

	public static void cadastrarBicicleta(Context ctx) {
		// o status deve ser "nova" (não pode ser editado) -- NÃO DESENVOLVIDO
		// o numero deve ser gerado pelo proprio sistema e nao pode ser editado (?) -- NÃO DESENVOLVIDO
		try {
			ctx.bodyValidator(ControllerBicicleta.class)
				.check(obj -> obj.marca != null && !obj.marca.trim().isEmpty())
				.check(obj -> obj.modelo != null && !obj.modelo.trim().isEmpty())
				.check(obj -> obj.ano != null && !obj.ano.trim().isEmpty())
				.check(obj -> obj.status != null && !obj.status.trim().isEmpty())
				.check(obj -> obj.numero != 0)
			    .get();

			ControllerBicicleta.bicicletas.add(ctx.bodyAsClass(ControllerBicicleta.class));

			ctx.status(200).json(ControllerBicicleta.bicicletas.get(ControllerBicicleta.bicicletas.size() - 1));
		} catch (Exception e) {
			ctx.status(422).result(MensagemResposta.DADOS_INVALIDOS.mensagem);
		}
    }

	public static void intregarBicicleta(Context ctx) {
		// a bike teve ter status "nova" ou "em reparo" -- NÃO DESENVOLVIDO
		// a tranca deve ter status "disponivel" -- NÃO DESENVOLVIDO
		try {
			ctx.bodyValidator(BicicletaTranca.class)
				.check(obj -> obj.idTranca != null && !obj.idTranca.trim().isEmpty())
				.check(obj -> obj.idBicicleta != null && !obj.idBicicleta.trim().isEmpty())
				.get();

			BicicletaTranca requestBody = ctx.bodyAsClass(BicicletaTranca.class);
			ControllerTranca tranca = ControllerTranca.obterTrancaPorId(requestBody.idTranca);

			if (tranca != null && tranca.bicicleta == null) {
				ControllerBicicleta bicicleta = ControllerBicicleta.obterBicicletaPorId(requestBody.idBicicleta);

				if (bicicleta != null) {
					int indexTranca = ControllerTranca.trancas.indexOf(tranca);
					int indexBicicleta = ControllerBicicleta.bicicletas.indexOf(bicicleta);

					tranca.bicicleta = bicicleta.id;
					bicicleta.tranca = tranca.id;

					ControllerBicicleta.bicicletas.set(indexBicicleta, bicicleta);
					ControllerTranca.trancas.set(indexTranca, tranca);

					ctx.status(200).result("Dados cadastrados");
					return;
				}
			}
			ctx.status(422).result(MensagemResposta.DADOS_INVALIDOS.mensagem);
		} catch (Exception e) {
			ctx.status(422).result(MensagemResposta.DADOS_INVALIDOS.mensagem);
		}
    }

	public static void retirarBicicleta(Context ctx) {
		// a bike deve estar em uma tranca -- OK
		// e com o status "reparo solicitado" -- NÃO DESENVOLVIDO
		try {
			ctx.bodyValidator(BicicletaTranca.class)
				.check(obj -> obj.idTranca != null && !obj.idTranca.trim().isEmpty())
				.check(obj -> obj.idBicicleta != null && !obj.idBicicleta.trim().isEmpty())
				.get();

			BicicletaTranca requestBody = ctx.bodyAsClass(BicicletaTranca.class);
			ControllerTranca tranca = ControllerTranca.obterTrancaPorId(requestBody.idTranca);
			ControllerBicicleta bicicleta = ControllerBicicleta.obterBicicletaPorId(requestBody.idBicicleta);

			if (tranca != null && bicicleta != null && bicicleta.tranca != null 
				&& tranca.bicicleta.equals(requestBody.idBicicleta) 
				&& bicicleta.tranca.equals(requestBody.idTranca)) {	
				int indexTranca = ControllerTranca.trancas.indexOf(tranca);
				int indexBicicleta = ControllerBicicleta.bicicletas.indexOf(bicicleta);

				tranca.bicicleta = null;
				bicicleta.tranca = null;

				ControllerBicicleta.bicicletas.set(indexBicicleta, bicicleta);
				ControllerTranca.trancas.set(indexTranca, tranca);

				ctx.status(200).result("Dados cadastrados");
				return;
			}
			ctx.status(422).result(MensagemResposta.DADOS_INVALIDOS.mensagem);
		} catch (Exception e) {
			ctx.status(422).result(MensagemResposta.DADOS_INVALIDOS.mensagem);
		}
    }

	public static void obterBicicleta(Context ctx) {
		ControllerBicicleta bicicleta = ControllerBicicleta.obterBicicletaPorId(ctx.pathParam(ID_BICICLETA));

		if (bicicleta != null) {
			ctx.json(bicicleta).status(200);
			return;
		}
		ctx.status(404).result(MensagemResposta.NAO_ENCONTRADO.mensagem);
    }

	public static void editarBicicleta(Context ctx) {
		try {
			ctx.bodyValidator(ControllerBicicleta.class)
				.check(obj -> obj.marca != null && !obj.marca.trim().isEmpty())
				.check(obj -> obj.modelo != null && !obj.modelo.trim().isEmpty())
				.check(obj -> obj.ano != null && !obj.ano.trim().isEmpty())
				.check(obj -> obj.status != null && !obj.status.trim().isEmpty())
				.check(obj -> obj.numero != 0)
			    .get();

			ControllerBicicleta bicicleta = ControllerBicicleta.obterBicicletaPorId(ctx.pathParam(ID_BICICLETA));

			if (bicicleta != null) {
				int index = ControllerBicicleta.bicicletas.indexOf(bicicleta);
				String idBicicleta = bicicleta.id;

				bicicleta = ctx.bodyAsClass(ControllerBicicleta.class);
				bicicleta.id = idBicicleta;

				ControllerBicicleta.bicicletas.set(index, bicicleta);

				ctx.status(200).json(bicicleta);
				return;
			}
	        ctx.status(404).result(MensagemResposta.NAO_ENCONTRADO.mensagem);
		} catch (Exception e) {
			ctx.status(422).result(MensagemResposta.DADOS_INVALIDOS.mensagem);
		}
    }

	public static void removerBicicleta(Context ctx) {
		// apenas bikes com status "aposentada" -- NÃO DESENVOLVIDO
		// e não estiverem em nenhuma tranca podem ser excluidas -- OK
		ControllerBicicleta bicicleta = ControllerBicicleta.obterBicicletaPorId(ctx.pathParam(ID_BICICLETA));

		if (bicicleta != null && bicicleta.tranca == null) {
			ControllerBicicleta.bicicletas.remove(bicicleta);

			ctx.status(200).result("Dados removidos");
			return;
		}
		ctx.status(404).result(MensagemResposta.NAO_ENCONTRADO.mensagem);
    }

	public static void statusBicicleta(Context ctx) {
		ControllerBicicleta bicicleta = ControllerBicicleta.obterBicicletaPorId(ctx.pathParam(ID_BICICLETA));

		if (bicicleta != null) {
			int index = ControllerBicicleta.bicicletas.indexOf(bicicleta);

			for (BicicletaStatus status : BicicletaStatus.values()) {
		        if (status.toString().equals(ctx.pathParam("acao").toUpperCase())) {
		        	bicicleta.status = status.toString();
		        	ControllerBicicleta.bicicletas.set(index, bicicleta);
		        	ctx.status(200).result("Ação bem sucedida");
		        	return;
		        }
		    }
			ctx.status(422).result(MensagemResposta.DADOS_INVALIDOS.mensagem);
			return;
		}
        ctx.status(404).result(MensagemResposta.NAO_ENCONTRADO.mensagem);
    }

	// Funções auxiliares

	public static ControllerBicicleta obterBicicletaPorId(String idBicicleta) {
		for (ControllerBicicleta bicicleta : bicicletas) {
			if (bicicleta.id.equals(idBicicleta)) {
				return bicicleta;
			}
		}
		return null;
	}
}
