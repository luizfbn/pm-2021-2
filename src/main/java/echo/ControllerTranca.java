package echo;

import java.util.ArrayList;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.javalin.http.Context;

public class ControllerTranca {

	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	public String id = UUID.randomUUID().toString();
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	public String bicicleta;

	public String localizacao;
	public String anoDeFabricacao;
	public String modelo;
	public String status;
	public int numero;
	
	private static final String ID_TRANCA = "idTranca";
	
	public static ArrayList <ControllerTranca> trancas = new ArrayList <ControllerTranca>();

	public ControllerTranca(
			@JsonProperty("localizacao") String localizacao, 
			@JsonProperty("anoDeFabricacao") String anoDeFabricacao, 
			@JsonProperty("modelo") String modelo, 
			@JsonProperty("status") String status, 
			@JsonProperty("numero") int numero) {
		this.localizacao = localizacao;
		this.anoDeFabricacao = anoDeFabricacao;
		this.modelo = modelo;
		this.status = status;
		this.numero = numero;
	}

	public static void recuperarTrancas(Context ctx) {
		ctx.status(200).json(ControllerTranca.trancas);
    }

	public static void cadastrarTranca(Context ctx) {
		// o status deve ser "nova" (não pode ser editado) -- NÃO DESENVOLVIDO
		try {
			ctx.bodyValidator(ControllerTranca.class)
				.check(obj -> obj.localizacao != null && !obj.localizacao.trim().isEmpty())
				.check(obj -> obj.anoDeFabricacao != null && !obj.anoDeFabricacao.trim().isEmpty())
				.check(obj -> obj.modelo != null && !obj.modelo.trim().isEmpty())
				.check(obj -> obj.status != null && !obj.status.trim().isEmpty())
				.check(obj -> obj.numero != 0)
			    .get();

			ControllerTranca.trancas.add(ctx.bodyAsClass(ControllerTranca.class));

			ctx.status(200).json(ControllerTranca.trancas.get(ControllerTranca.trancas.size() - 1));
		} catch (Exception e) {
			ctx.status(422).result(MensagemResposta.DADOS_INVALIDOS.mensagem);
		}
    }

	public static void integrarTranca(Context ctx) {
		// a tranca deve estar com status de "nova" ou "em reparo" -- NÃO DESENVOLVIDO
		try {
			ctx.bodyValidator(TrancaTotem.class)
				.check(obj -> obj.idTotem != null)
				.check(obj -> obj.idTranca != null)
				.get();

			TrancaTotem requestBody = ctx.bodyAsClass(TrancaTotem.class);
			ControllerTotem totem = ControllerTotem.obterTotemPorId(requestBody.idTotem);
			ControllerTranca tranca = ControllerTranca.obterTrancaPorId(requestBody.idTranca);

			if (totem != null && tranca != null && totem.trancasTotem.size() < 20 && !totem.trancasTotem.contains(tranca)) {
				int index = ControllerTotem.totens.indexOf(totem);

				totem.trancasTotem.add(tranca);
				ControllerTotem.totens.set(index, totem);

				ctx.status(200).result("Dados cadastrados");
				return;
			}
			ctx.status(422).result(MensagemResposta.DADOS_INVALIDOS.mensagem);
		} catch (Exception e) {
			ctx.status(422).result(MensagemResposta.DADOS_INVALIDOS.mensagem);
		}
    }

	public static void retirarTranca(Context ctx) {
		// a tranca nao pode ter bikes nela -- OK
		try {
			ctx.bodyValidator(TrancaTotem.class)
				.check(obj -> obj.idTotem != null && !obj.idTotem.trim().isEmpty())
				.check(obj -> obj.idTranca != null && !obj.idTranca.trim().isEmpty())
				.get();

			TrancaTotem requestBody = ctx.bodyAsClass(TrancaTotem.class);
			ControllerTotem totem = ControllerTotem.obterTotemPorId(requestBody.idTotem);
			ControllerTranca tranca = ControllerTranca.obterTrancaPorId(requestBody.idTranca);

			if (totem != null && tranca != null && tranca.bicicleta == null && totem.trancasTotem.contains(tranca)) {	
				totem.trancasTotem.remove(tranca);

				ctx.status(200).result("Dados cadastrados");
				return;
			}
			ctx.status(422).result(MensagemResposta.DADOS_INVALIDOS.mensagem);
		} catch (Exception e) {
			ctx.status(422).result(MensagemResposta.DADOS_INVALIDOS.mensagem);
		}
    }

	public static void obterTranca(Context ctx) {
		ControllerTranca tranca = ControllerTranca.obterTrancaPorId(ctx.pathParam(ID_TRANCA));

		if (tranca != null) {
			ctx.status(200).json(tranca);
			return;
		}
		ctx.status(404).result(MensagemResposta.NAO_ENCONTRADO.mensagem);
    }

	public static void editarTranca(Context ctx) {
		try {
			ctx.bodyValidator(ControllerTranca.class)
				.check(obj -> obj.localizacao != null && !obj.localizacao.trim().isEmpty())
				.check(obj -> obj.anoDeFabricacao != null && !obj.anoDeFabricacao.trim().isEmpty())
				.check(obj -> obj.modelo != null && !obj.modelo.trim().isEmpty())
				.check(obj -> obj.status != null && !obj.status.trim().isEmpty())
				.check(obj -> obj.numero != 0)
			    .get();

			ControllerTranca tranca = ControllerTranca.obterTrancaPorId(ctx.pathParam(ID_TRANCA));

			if (tranca != null) {
				int index = ControllerTranca.trancas.indexOf(tranca);
				String idTranca = tranca.id;
	
				tranca = ctx.bodyAsClass(ControllerTranca.class);
				tranca.id = idTranca;
	
				ControllerTranca.trancas.set(index, tranca);

				ctx.status(200).json(tranca);
				return;
			}
	        ctx.status(404).result(MensagemResposta.NAO_ENCONTRADO.mensagem);
		} catch (Exception e) {
			ctx.status(422).result(MensagemResposta.DADOS_INVALIDOS.mensagem);
		}
    }

	public static void removerTranca(Context ctx) {
		// apenas trancas sem bikes podem ser excluidas -- OK
		ControllerTranca tranca = ControllerTranca.obterTrancaPorId(ctx.pathParam(ID_TRANCA));

		if (tranca != null && tranca.bicicleta == null) {
			ControllerTranca.trancas.remove(tranca);
			
			ctx.status(200).result("Tranca removida");
			return;
		}
		ctx.status(404).result(MensagemResposta.NAO_ENCONTRADO.mensagem);
    }

	public static void statusTranca(Context ctx) {
		ControllerTranca tranca = ControllerTranca.obterTrancaPorId(ctx.pathParam(ID_TRANCA));

		if (tranca != null) {
			int index = ControllerTranca.trancas.indexOf(tranca);

			for (TrancaStatus status : TrancaStatus.values()) {
		        if (status.toString().equals(ctx.pathParam("acao").toUpperCase())) {
		        	tranca.status = status.toString();
		        	ControllerTranca.trancas.set(index, tranca);
		        	ctx.status(200).result("Ação bem sucedida");
		        	return;
		        }
		    }
			ctx.status(422).result(MensagemResposta.DADOS_INVALIDOS.mensagem);
			return;
		}
        ctx.status(404).result(MensagemResposta.NAO_ENCONTRADO.mensagem);
    }

	public static void obterBicicleta(Context ctx) {
		ControllerTranca tranca = ControllerTranca.obterTrancaPorId(ctx.pathParam(ID_TRANCA));

		if (tranca != null) {
			ControllerBicicleta bicicleta = ControllerBicicleta.obterBicicletaPorId(tranca.bicicleta);
			if (bicicleta != null) {
				ctx.status(200).json(bicicleta);
				return;
			}
		}
		ctx.status(404).result(MensagemResposta.NAO_ENCONTRADO.mensagem);
    }

	// Funções auxiliares

	public static ControllerTranca obterTrancaPorId(String idTranca) {
		for (ControllerTranca tranca : trancas) {
			if (tranca.id.equals(idTranca)) {
				return tranca;
			}
		}
		return null;
	}
}
